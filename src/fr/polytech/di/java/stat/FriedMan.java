package fr.polytech.di.java.stat;

import java.util.ArrayList;

public class FriedMan {
	
	ArrayList<ArrayList<Double>> listeGroupe = new ArrayList<ArrayList<Double>>();
	ArrayList<Double> groupes = new ArrayList<Double>();
	double Q;
	double somme;
	
	
	public FriedMan(ArrayList<ArrayList<Double>> listeGroupe) {
		this.listeGroupe=listeGroupe;
		Q = 0;
		somme = 0;
	}
	
	public void test() {
		// TODO Auto-generated method stub
		groupes=Rang.calculRang(listeGroupe);
		System.out.println(groupes);


		int iteration = 0;
		for(ArrayList<Double>groupe:listeGroupe) {
			somme += groupes.get(iteration) - ((listeGroupe.size()*(groupe.size()+1))/2);
			iteration++;
		}
		
		if(listeGroupe != null & listeGroupe.size() != 0)
			Q += (12/(listeGroupe.size()*listeGroupe.get(0).size()*(listeGroupe.get(0).size()+1))) + somme;
		System.out.println("Q : "+Q);
		
		
	}

}
