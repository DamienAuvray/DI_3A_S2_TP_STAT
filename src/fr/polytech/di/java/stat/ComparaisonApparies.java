package fr.polytech.di.java.stat;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class ComparaisonApparies {
	
	Scanner sc = new Scanner(System.in);

	private ArrayList<Double> echantillon1;
	private ArrayList<Double> echantillon2;
	private ArrayList<Double> echantillonDifferences;
	private ArrayList<Double> echantillonDifferencesM;
	private ArrayList<Double> echantillonDifferencesP;

	private int W_Moins;
	private int W_Plus;
	private int d = 0;
	

	public ComparaisonApparies(ArrayList<Double> echantillon1, ArrayList<Double> echantillon2) {
		if(echantillon1.size() != echantillon2.size())
			throw new IllegalArgumentException();
		
		this.echantillon1 = echantillon1;
		this.echantillon2 = echantillon2;
	}
	
	public void test(){
		
		System.out.println("Choisissez un test : \n");
		System.out.println("Saisissez 1 pour : ");
		System.out.println("H0 : nuh=0");
		System.out.println("H1 : nuh!=0\n");
		
		System.out.println("Saisissez 2 pour : ");
		System.out.println("H0 : nuh=0");
		System.out.println("H1 : nuh>0\n");
		
		System.out.println("Saisissez 3 pour : ");
		System.out.println("H0 : nuh=0");
		System.out.println("H1 : nuh<0\n");
		
		d = sc.nextInt();

		echantillonDifferences = new ArrayList<>();
		echantillonDifferencesM = new ArrayList<>();
		echantillonDifferencesP = new ArrayList<>();
		
		for(int i = 0; i<echantillon1.size(); i++){
			echantillonDifferences.add(Math.abs(echantillon1.get(i)-echantillon2.get(i)));

			if(echantillon1.get(i)-echantillon2.get(i) < 0)
				echantillonDifferencesM.add(Math.abs(echantillon1.get(i)-echantillon2.get(i)));
			else
				echantillonDifferencesP.add(Math.abs(echantillon1.get(i)-echantillon2.get(i)));			
		}
		
		Collections.sort(echantillonDifferences);
		
		for(int i = 0; i<echantillonDifferences.size(); i++){

			if(echantillonDifferencesM.contains(echantillonDifferences.get(i)))
					W_Moins += (i+1);
			else if(echantillonDifferencesP.contains(echantillonDifferences.get(i)))
					W_Plus += (i+1);
		}
		if(d==1){
			System.out.println("Veuillez saisir d : ");
			d = sc.nextInt();
			
			if(W_Moins < d || W_Plus < d)
				System.out.println("rejet de H0");
			else
				System.out.println("acceptation de H0");
		}else if(d==2){
			System.out.println("Veuillez saisir d : ");
			d = sc.nextInt();
			
			if(W_Moins < d)
				System.out.println("rejet de H0");
			else
				System.out.println("acceptation de H0");
		}else if(d==3){
			System.out.println("Veuillez saisir d : ");
			d = sc.nextInt();
			
			if(W_Plus < d)
				System.out.println("rejet de H0");
			else
				System.out.println("acceptation de H0");
		}else{
			System.out.println("Erreur de saisie...");
		}
		
		
		
	}
	
}
