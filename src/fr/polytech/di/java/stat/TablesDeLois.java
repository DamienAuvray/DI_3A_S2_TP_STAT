package fr.polytech.di.java.stat;

import org.apache.commons.math3.distribution.ChiSquaredDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.TDistribution;

public class TablesDeLois {
	
	private static NormalDistribution d = new NormalDistribution(0, 1);
	
	public static double tableC(double value, String arg) {
		double result = 0;
		if(arg.equals("lambda"))
			result=d.inverseCumulativeProbability(1-(1-value)/2);
		if(arg.equals("alphaPrime"))
			result=d.inverseCumulativeProbability(1-value);
		if(arg.equals("alphaPrimePrime"))
			result=d.inverseCumulativeProbability(1-value/2);
		return result;
	}
	
	public static double tableE(double n, double z) {
		return 0.5*(n+1-z*Math.sqrt(n));
	}
	
	public static double tableF(double n, double z) {
		return 0.5*(0.5*n*(n+1)+1-z*Math.sqrt(n*(n+1)*(2*n+1)/6));
	}
	
	public static double tableG(double m, double n, double z) {
		return 0.5*(m*n+1-z*Math.sqrt(m*n*(m+n+1)/3));
	}
	
	public static double tableChi2(double v, double alphaRisque) {
		ChiSquaredDistribution x2 = new ChiSquaredDistribution( v );
		return x2.inverseCumulativeProbability(1-alphaRisque);
	}
	
	public static double tableT(double v, double alpha) {
		TDistribution  t = new TDistribution (v);
		return t.inverseCumulativeProbability(1-alpha/2);
	}
	
	public static void stub() {

		System.out.println(
		TablesDeLois.tableC(0.95, "lambda")+"\n"+
		TablesDeLois.tableC(0.025, "alphaPrime")+"\n"+
		TablesDeLois.tableC(0.05, "alphaPrimePrime")+"\n"
		);
		System.out.println(
		TablesDeLois.tableE(39, TablesDeLois.tableC(0.947, "lambda"))+"\n"+
		TablesDeLois.tableE(39, TablesDeLois.tableC(0.027, "alphaPrime"))+"\n"+
		TablesDeLois.tableE(39, TablesDeLois.tableC(0.053, "alphaPrimePrime"))+"\n"
		);
		System.out.println(
		TablesDeLois.tableF(14, TablesDeLois.tableC(0.951, "lambda"))+"\n"+
		TablesDeLois.tableF(14, TablesDeLois.tableC(0.025, "alphaPrime"))+"\n"+
		TablesDeLois.tableF(14, TablesDeLois.tableC(0.049, "alphaPrimePrime"))+"\n"
		);
		System.out.println(
		TablesDeLois.tableG(5, 7,  TablesDeLois.tableC(0.952, "lambda"))+"\n"+
		TablesDeLois.tableG(5, 7,  TablesDeLois.tableC(0.024, "alphaPrime"))+"\n"+
		TablesDeLois.tableG(5, 7,  TablesDeLois.tableC(0.048, "alphaPrimePrime"))+"\n"
		);
		System.out.println(TablesDeLois.tableChi2(7, 0.99));
		System.out.println(TablesDeLois.tableT(7, 0.01));
	}
}
