package fr.polytech.di.java.stat;

import java.util.ArrayList;

public class Rang implements Comparable<Rang>{
	private int numGroupe;
	private double value;
	private double rg;
	
	public Rang(int numGroupe, double value, int rg) {
        this.numGroupe = numGroupe;
        this.value = value;
        this.rg = rg;
    }
	
	public Rang(Rang rang) {
		// TODO Auto-generated constructor stub
		this.numGroupe = rang.numGroupe;
        this.value = rang.value;
        this.rg = rang.rg;
	}
	
	public static ArrayList<Double> calculRang(ArrayList<ArrayList<Double>> listeGroupe) {
		ArrayList<Rang> rang = new ArrayList<Rang>();
		ArrayList<Double> groupes = new ArrayList<Double>();
		int i=0;
		double compteur=0;
		for(ArrayList<Double> listeDouble: listeGroupe) {
			for(double d: listeDouble) {
				rang.add(new Rang(i, d, 0));
			}
			i++;
		}
		rang.sort(null);
		i=0;
		for(int k=0; k<rang.size(); k++) {
			if(k<=(rang.size()-2) 	&&	rang.get(k).equals(rang.get(k+1))	||	k!=0	&& 	rang.get(k).equals(rang.get(k-1))) {
				Rang rgBoucle = new Rang(rang.get(k));
				compteur = rang.stream().filter(r --> r.equals(rgBoucle)).count();
				rang.get(k).setRg(i+1+1/compteur);
			}
			else {
				i += compteur;
				compteur=0;
				i++;
				rang.get(k).setRg(i);
			}
		}
		for(int j=0;j<listeGroupe.size();j++) {
			double sommeRang=0;
			for(Rang rg: rang) {
				if(rg.getNumGroupe()==j)
					sommeRang+=rg.getRg();
			}
			groupes.add(sommeRang);
		}
		return groupes;
	}

	public void setRg(double d) {
		this.rg = d;
	}

	public int getNumGroupe() {
		return numGroupe;
	}

	public void setNumGroupe(int numGroupe) {
		this.numGroupe = numGroupe;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public double getRg() {
		return rg;
	}

	@Override
	public String toString() {
		return "Rang [numGroupe=" + numGroupe + ", value=" + value + ", rg=" + rg + "]";
	}

	@Override
	public int compareTo(Rang rg) {
		// TODO Auto-generated method stub
		return (int)(this.value-rg.value);
	}
	
	public boolean equals(Rang rg){
		if(this.value-rg.value==0)
			return true;
		else
			return false;
	}
}
