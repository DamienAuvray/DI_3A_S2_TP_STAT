package fr.polytech.di.java.stat;

import java.util.ArrayList;

public class KruskalWallis {
	
	ArrayList<ArrayList<Double>> listeGroupe = new ArrayList<ArrayList<Double>>();
	ArrayList<Double> groupes = new ArrayList<Double>();
	
	
	public KruskalWallis(ArrayList<ArrayList<Double>> listeGroupe) {
		// TODO Auto-generated constructor stub
		this.listeGroupe=listeGroupe;
	}


	public void test() {
		// TODO Auto-generated method stub
		groupes=Rang.calculRang(listeGroupe);

		double H=0;
		int total=0, iteration=0;;
		for(ArrayList<Double>groupe:listeGroupe)
			total+=groupe.size();
		for(ArrayList<Double>groupe:listeGroupe) {
			H+=(1.0/groupe.size())*Math.pow((groupes.get(iteration)-groupe.size()*(total+1.0)/2.0),2);
			iteration++;
		}
		H*=(12.0/(total*(total+1.0)));
		System.out.println(H);
	}

}
