import java.util.ArrayList;
import java.util.Arrays;

import fr.polytech.di.java.stat.*;

public class Main {
	public static void main(String[] args) {
		//TestComparaisonApparies();
		TestKruskalWallis();
		//TablesDeLois.stub();
		
	}
	
	static void TestComparaisonApparies(){
		ArrayList<Double> echantillon1 = new ArrayList<Double>();
		ArrayList<Double> echantillon2 = new ArrayList<Double>();
		echantillon1.addAll(Arrays.asList(90.4, 71.9, 102.5, 83.4, 98.3, 75.7, 67.1, 74.1));
		echantillon2.addAll(Arrays.asList(89.1, 71.1, 93.1, 77.6, 98.9, 71.2, 68.3, 70.3));
		ComparaisonApparies c = new ComparaisonApparies(echantillon1, echantillon2);
		c.test();
	}
	
	static void TestKruskalWallis(){
		ArrayList<Double> groupe1 = new ArrayList<Double>();
		ArrayList<Double> groupe2 = new ArrayList<Double>();
		ArrayList<Double> groupe3 = new ArrayList<Double>();
		groupe1.addAll(Arrays.asList(66.0, 68.0, 80.0, 72.0, 74.0, 57.0, 75.0));
		groupe2.addAll(Arrays.asList(77.0, 68.0, 67.0, 85.0, 89.0, 74.0, 61.0));
		groupe3.addAll(Arrays.asList(79.0, 83.0, 78.0, 92.0, 98.0, 69.0, 91.0));
		ArrayList<ArrayList<Double>> listeGroupe = new ArrayList<ArrayList<Double>>();
		listeGroupe.add(groupe1);
		listeGroupe.add(groupe2);
		listeGroupe.add(groupe3);
		KruskalWallis k = new KruskalWallis(listeGroupe);
		k.test();
	}

}
